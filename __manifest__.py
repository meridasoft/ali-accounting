# -*- coding: utf-8 -*-
{
    'name': 'ali-accounting',
    'version': '1.0',
    'description': 'Diversas Herramientas para complementar la contabilidad de Odoo para ALI',
    'summary': 'Diversas Herramientas para complementar la contabilidad de Odoo para ALI',
    'author': 'Meridasoft',
    'license': 'LGPL-3',
    'category': 'pos',
    'depends': [
        #'res',
        'base',
        'account',
        'sale',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/account_move.xml',
        'views/menu.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}