# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api
import datetime
from datetime import date
import logging
from odoo.exceptions import AccessError, UserError, ValidationError
_logger = logging.getLogger(__name__)

class ALI_SaleAdvancePaymentInv(models.TransientModel):
    _inherit = 'sale.advance.payment.inv'
    deduct_down_payments = fields.Boolean('Deduct down payments', default=False)

class Ali_AccountMove(models.Model):
    _inherit = 'account.move'
    advance_lines = fields.One2many ('aliaccount.advancelines', "validator_id", string ='Lineas de Anticipos')
    inv_type      = fields.Selection([('a','Factura Anticipo'),
                                      ('t','Factura Total'),
                                     ],string='Tipo de Factura de Venta',default='a')

    #crea una nota de credito a partir de los anticipos sin asignar                                     
    def create_ncred(self):
        #por cualquier cosa primero actualizamos los anticipos
        self.advances_rel()
        #Rellenamos con los valores por default
        today = date.today()
        lines = []
        vals = {}
        for ant in self.advance_lines:#por cada anticipo
            if len(ant.ncred_name) != 0:#si no tiene asignado una nota de credito entonces se considerará
                continue
            for a_line in ant.adv_id.invoice_line_ids:#por cada linea de factura de cada anticipo
                    line_ant = {
                        'product_id'            :a_line.product_id.id,
                        'quantity'              :a_line.quantity,
                        'price_unit'            :a_line.price_unit,
                        'tax_ids'               :[(6, 0,a_line.tax_ids.ids)],
                        'name'                  :a_line.name,
                        'account_id'            :a_line.account_id.id,
                        'product_uom_id'        :a_line.product_uom_id.id,
                    }
                    lines.append((0,0,line_ant))
            #_logger.error('---Contenido de lineas de anticipos --->>'+str(lines))
            vals = {
                'state'                         : 'draft',
                'partner_id'                    : self.partner_id.id,
                'partner_shipping_id'           : self.partner_shipping_id,
                'l10n_mx_edi_payment_method_id' : self.l10n_mx_edi_payment_method_id.id,
                'l10n_mx_edi_payment_policy'    : self.l10n_mx_edi_payment_policy,
                'l10n_mx_edi_usage'             : self.l10n_mx_edi_usage,
                'l10n_mx_edi_cfdi_uuid'         : self.l10n_mx_edi_cfdi_uuid,
                'invoice_date'                  : today,
                'move_type'                     : 'out_refund',    #cambiamos el tipo de factura a nota de crédito
                'invoice_line_ids'              : lines,#eliminamos las lineas default ya que se crearan a mano
                'advance_lines'                 : '' , #no tiene sentido que se hereden los anticipos
                'inv_type'                      : 'a' #no marcamos como t para que no le aparezca el boton de anticipos
            }
        #_logger.error('--------> Contenido de Vals para nota de credito'+str(vals))
        if len(vals) == 0:# no hay anticipos sin nota de crédito
            raise ValidationError('No hay Anticipos sin Nota de Crédito Asignada, nada por crear')
        if len(vals) >0:#si vals tiene valotes entonces damos de alta la nota de crédito
            poliza_id = self.env['account.move'].create(vals)
            #ahora pegamos el id de la nota de credito er las posiciones vacias
            for ant in self.advance_lines:#por cada anticipo
                if len(ant.ncred_name) == 0:#si no tiene asignado una nota de credito entonces se le asigna el id
                    ant.ncred_name = poliza_id.id
            _logger.error('--------> Poliza creada '+str(poliza_id.id))
        return self

    def advances_rel(self):
        """[summary]

        Raises:
            ValidationError: [description]

        Returns:
            [type]: [description]
        """
        #si es una factura tipo anticipo no puede generar listado de anticipos
        if self.inv_type == 'a':
            raise ValidationError('El listado de anticipos solo aplica para Facturas Totales')

        if len(str(self.invoice_origin)) > 0: #es una referencia válida???
            advances = self.env['account.move'].search([ ('invoice_origin', '=', str(self.invoice_origin) ), ('state', '!=', 'draft'), ('move_type', '=', 'out_invoice')])
            if len(advances) <= 0: #no hay facturas relacionadas vamonos
                return self
            for a in advances:
                #verificamos que no este ya en la tabla
                flag = False
                if a.inv_type == 't':
                    flag = True #solo se agregan facturas tipo anticipo
                for l in self.advance_lines:
                    if a.name == l.name:
                        flag = True #ya esta en la lista no vale la pena agregar
                    if l.ncred_name.state == 'cancel':#cancelaron la nota de credito =???
                        l.ncred_name = False#limpiamos el campo
                if flag == False:# es una factura nueva hay que agregarla
                    values={
                        'validator_id'   : self.id,
                        'adv_id'         : a.id,
                        'name'           : a.name,
                        'payment_state'  : a.payment_state,
                        'invoice_date'   : a.invoice_date,
                        'invoice_origin' : a.invoice_origin,
                        'amount_total'   : a. amount_total,
                        'l10n_mx_edi_cfdi_uuid' :a.l10n_mx_edi_cfdi_uuid,
                    }
                    self.advance_lines.create(values)
        
class Ali_AdvanceLines(models.Model):
    _name = 'aliaccount.advancelines'
    _description = "Anticipos"
    _rec_name = 'name'
    PAYMENT_STATE_SELECTION = [
        ('not_paid', 'Sin Pagar'),
        ('in_payment', 'En proceso de Pago'),
        ('paid', 'Pagado'),
        ('partial', 'Pagado Parcialmente'),
        ('reversed', 'Reversado'),
        ('invoicing_legacy', 'Invoicing App Legacy'),
]
    validator_id =      fields.Many2one("account.move")
    adv_id =            fields.Many2one("account.move")
    name =              fields.Char(string ='Documento')
    payment_state =     fields.Selection(PAYMENT_STATE_SELECTION, string="Status de Pago")
    invoice_date =      fields.Date(string='Fecha')
    invoice_origin =    fields.Char(string ='Documento Origen')
    amount_total =      fields.Float(string ='Importe')   
    l10n_mx_edi_cfdi_uuid= fields.Char(string='UUID CFDI') 
    ncred_name =        fields.Many2one("account.move", string ='Nota de Crédito Asociada')


    
