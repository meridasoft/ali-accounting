# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api
import datetime
import logging
_logger = logging.getLogger(__name__)


class Ali_SaleOrder(models.Model):
    _inherit = "sale.order"

    def _prepare_invoice(self):
        vals = super(Ali_SaleOrder, self)._prepare_invoice()
        #agregamos al diccionario el campo tipo de factura
        vals["inv_type"] = 't'
        return vals